import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, FlatList, Text } from 'react-native';

import firebase from '../firebase/firebaseConfig';

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            userArr: []
        }
        this.firestoreRef = firebase.firestore().collection('user');
    }
    componentDidMount() {
        this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
    }
    getCollection = (querySnapshot) => {
        const userArr = [];
        querySnapshot.forEach((res) => {
            const { name, email, mobile } = res.data();
            userArr.push({
                key: res.id,
                name,
                email,
            });
        });
        this.setState({
            userArr,
            isLoading: false,
        });
       
    }

    renderItem = ({ item, index }) => (
        <View style={styles.item}>
            <Text style={styles.title}>{item.name}</Text>
            <Text>{item.email}</Text>
        </View>
    );

    render() {

        if (this.state.isLoading) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            )
        }
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.userArr}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.key}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:10
        //paddingBottom: 22
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    item: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'gray',
        padding: 10,
        marginVertical: 3,
        marginHorizontal: 5,
    },
    title: {
        fontSize: 18,
    },
})