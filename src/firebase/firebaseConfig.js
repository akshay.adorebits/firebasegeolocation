// database/firebaseDb.js

import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCJQ9WguDY-BCoXOuOqi90kbLBAos-AoRw",
    authDomain: "crud-app-122f5.firebaseapp.com",
    databaseURL: "https://crud-app-122f5.firebaseio.com",
    projectId: "crud-app-122f5",
    storageBucket: "crud-app-122f5.appspot.com",
    messagingSenderId: "738603959240",
    appId: "1:738603959240:web:913457f249fa501b1936fd",
    measurementId: "G-KKJSED9R1J"
};

firebase.initializeApp(firebaseConfig);

firebase.firestore();

export default firebase;