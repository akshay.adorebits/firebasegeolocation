import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { GoogleSignin } from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';

/** screens */
import LoginScreen from '../screens/LoginScreen';
import UserInfoScreen from '../screens/UserInfoScreen';
import MyTabs from './TabNavigation';
import { Button, ActivityIndicator, View } from 'react-native';

const Stack = createStackNavigator();

export default class AppNavigation extends React.Component {
    state = {
        loading: false
    }

    signOut = async (navigation) => {
        
        this.setState({ loading: true })
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();

            await AsyncStorage.removeItem('id').then(() => {
                this.setState({ loading: false })
                
                navigation.navigate("Login");
            })
        } catch (error) {
            console.error(error);
        }
    };

    render() {
        const user = this.props.user;
        const userExist = this.props.userExist;

        console.log("userid =>",user);
        console.log("userexist =>",userExist);
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            )
        }
        return (
            <NavigationContainer>
                <Stack.Navigator>
                {/* {!user ?  */}
                        <Stack.Screen
                            name="Login"
                            component={LoginScreen}
                            options={{ headerShown: false }}
                        />
                     {/* : 
                    user && userExist==false ?  */}
                    
                    <Stack.Screen
                        name="UserInfoScreen"
                        component={UserInfoScreen}
                        options={{ title: 'User Detail' }}
                    />
                   {/* :   */}
                    <Stack.Screen
                        name="MyTabs"
                        component={MyTabs}
                        options={({ navigation }) => ({
                            title: 'Welcome',
                            headerLeft: null,
                            headerRight: () => (
                                <Button
                                    onPress={() => this.signOut(navigation)}
                                    title="Signout"

                                />
                            ),
                        })}
                    />
                 {/* }  */}
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}