import firebase from './firebaseConfig';
import AsyncStorage from '@react-native-community/async-storage';

export default checkUser = (userid) => {
    
    const dbref = firebase.firestore().collection('user').doc(userid);
    return dbref.get().then((res) => {
        const user = res.data();
        if(user){
            return true;
        }
    }).catch((err) => {
        console.log("error =>", err);
        return false;
    })
}