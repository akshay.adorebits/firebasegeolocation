import React, { Component } from 'react';
import { View, Text, Dimensions,StyleSheet,FlatList } from 'react-native';
import { Picker } from '@react-native-community/picker';
import firebase from '../firebase/firebaseConfig';
import * as geofirex from 'geofirex';
import { get } from 'geofirex';
//import * as geofirex from 'geofire'; 
import { firestore } from 'firebase';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from '@react-native-community/geolocation';
import { BehaviorSubject } from 'rxjs';

export default class SearchScreen extends Component {

    state = {
        distance: null,
        data: []
    }
    constructor(props) {
        super(props)
        Geolocation.getCurrentPosition(

            (position) => {
                const currentLongitude = position.coords.longitude;
                const currentLatitude = position.coords.latitude;

                this.setState({currentLongitude:currentLongitude})
                this.setState({currentLatitude:currentLatitude})
            },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );

       // radius = BehaviorSubject(5)
    }
    _getUserList= async(itemValue) =>{
        this.setState({ distance: itemValue })
        alert(itemValue)
       // console.log("call ==>",this.state.currentLongitude)
        const geo = geofirex.init(firebase);

        const user = firestore().collection('user');
        const center = geo.point(this.state.currentLatitude, this.state.currentLongitude);
        const radius = this.state.distance;
        const field = 'position';

        query = geo.query(user).within(center, radius, field);  
       // query.subscribe()
       const hits = await get(query);

       this.setState({data:hits})
       console.log(this.state.data)
  

    }
    renderItem = ({ item, index }) => (
        <View style={styles.item}>
            <Text style={styles.title}>{item.name}</Text>
            <Text>{item.email}</Text>
        </View>
    );
    render() {
       
        return (
            <View style={{ flex: 1, marginHorizontal: 1 }}>
                <Picker
                    selectedValue={this.state.distance}
                    style={{ height: 50, width: Dimensions.width }}
                    onValueChange={(itemValue) =>

                        this._getUserList(itemValue)
                    }>
                    <Picker.Item label="Select Distance" value="null" />
                    <Picker.Item label="5 Km" value="5" />
                    <Picker.Item label="10 Km" value="10" />
                    <Picker.Item label="15 Km" value="15" />
                </Picker>
                <View style={styles.container}>
                    <FlatList
                        data={this.state.data}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.key}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:150
        //paddingBottom: 22
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    item: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'gray',
        padding: 10,
        marginVertical: 3,
        marginHorizontal: 5,
    },
    title: {
        fontSize: 18,
    },
})