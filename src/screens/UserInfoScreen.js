import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Platform, Alert } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
//import Geocoder from 'react-native-geocoding';
import Geocoder from 'react-native-geocoder';
import firebase from '../firebase/firebaseConfig';
import * as geofirex from 'geofirex'; 
import AsyncStorage from '@react-native-community/async-storage';

export default class UserInfoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading :false,
            currentLongitude: 0,//Initial Longitude
            currentLatitude: 0,//Initial Latitude
        }

        this.ref = firebase.firestore().collection("user")
    }

    componentDidMount() {
        //const { userDetail } = this.props.route.params;
        AsyncStorage.getItem('id').then((value)=>this.setState({id:value}))
        AsyncStorage.getItem('name').then((value)=>this.setState({name:value}));
        AsyncStorage.getItem('email').then((value)=>this.setState({email:value}))
        //this.setState({ id:userDetail.user.id,name: userDetail.user.name, email: userDetail.user.email })
        var that = this;
        if (Platform.OS === 'ios') {
            this.callLocation(that);

        } else {

        }
    }

    callLocation(that) {
        //alert("callLocation Called");
        Geolocation.getCurrentPosition(
            
            (position) => {
              
                const currentLongitude = position.coords.longitude;
                const currentLatitude = position.coords.latitude;
                
                that.setState({ currentLongitude: currentLongitude });
                that.setState({ currentLatitude: currentLatitude });
               
                var NY = {
                    lat: this.state.currentLatitude,
                    lng: this.state.currentLongitude
                }
                
                Geocoder.geocodePosition(NY).then(res => {
                    this.setState({address:res[0].formattedAddress})
                })
                .catch(err => console.log(err))
                },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        
    }

    _saveData() {
        this.setState({loading : true });
       
        // this.ref.doc(this.state.id).set({
        //     name: this.state.name,
        //     email: this.state.email,
        //     geopoint : new firebase.firestore.GeoPoint(this.state.currentLatitude,this.state.currentLongitude)
            
        // }).then((res)=>{
        //         //console.log(res)
        //         this.setState({loading : false });
        //         alert("Register successfully..");
        //         this.props.navigation.navigate("MyTabs");
        //     })

        const geo = geofirex.init(firebase);
        this.ref.doc(this.state.id).set({
            name: this.state.name,
            email: this.state.email,
            address:this.state.address,
            position: geo.point(this.state.currentLatitude,this.state.currentLongitude)
        }).then((res)=>{
                
                    this.setState({loading : false });
                    alert("Register successfully..");
                    this.props.navigation.navigate("MyTabs");
                })
    }
    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            )
        }
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.textbox}
                    placeholder="Email"
                    value={this.state.name}
                    editable={false}
                />
                <TextInput
                    style={styles.textbox}
                    placeholder="Email"
                    value={this.state.email}
                    editable={false}
                />
                <TextInput
                    style={styles.textbox}
                    placeholder="Address"
                    value={this.state.address}
                    editable={false}
                />
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this._saveData()}>
                    <Text style={styles.btn}>Save</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        marginVertical: 10
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textbox: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        marginVertical: 5
    },
    button: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        marginVertical: 5,
        backgroundColor: '#347deb',
        borderColor: '#347deb'
    },
    btn: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18
    }
});