import React, { Component } from 'react';
import { View, Text,ActivityIndicator } from 'react-native';
import { GoogleSignin, GoogleSigninButton,statusCodes } from '@react-native-community/google-signin';
import firebase from '../firebase/firebaseConfig';
import AsyncStorage from '@react-native-community/async-storage';

export default class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      loading :false
    }
  }
    componentDidMount(){
        GoogleSignin.configure({
            webClientId: '738603959240-dbr1dlatgvkoe17v7mc143fv9vf1vohj.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            
          });
    }
    _signIn = async() =>{
        this.setState({loading:true})
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            
            AsyncStorage.setItem('id', userInfo.user.id);
            AsyncStorage.setItem('name', userInfo.user.name);
            AsyncStorage.setItem('email', userInfo.user.email);
            
           if(userInfo){

            const dbref = firebase.firestore().collection('user').doc(userInfo.user.id);
            dbref.get().then((res)=>{
              const user = res.data();
              
              if(user.email){
                this.setState({loading:false})
                this.props.navigation.navigate("MyTabs");
              }
            }).catch((err)=>{
              console.log("error =>",err);
              this.setState({loading:false})
              this.props.navigation.navigate("UserInfoScreen",{userDetail:userInfo});
            })
           }
          } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              this.setState({loading:false})
              // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
              // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              // play services not available or outdated
            } else {
              // some other error happened
              alert("Something was wrong")
              this.setState({loading:false})
            }
        }
    }
    render() {
      if(this.state.loading){
        return (
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <ActivityIndicator size="large" color="#9E9E9E"/>
          </View>
        )
      }
        return (
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <GoogleSigninButton
                    style={{ width: 192, height: 48 }}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this._signIn}
                    disabled={this.state.loading} 
                />
            </View>
        )
    }
}