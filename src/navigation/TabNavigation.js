import * as React from 'react';
import {Image} from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

//import Ionicons from 'react-native-vector-icons/Ionicons';

/** screen */
import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen';

const Tab = createBottomTabNavigator();
//const Tab = createMaterialBottomTabNavigator();
function MyTabs() {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    let src;
                    
                    if (route.name === 'HomeScreen') {
                        iconName = 'home';
                        src = require('../assets/tabIcon/home.png');
                    } else if (route.name === 'SearchScreen') {
                        iconName = 'search';
                        src = require('../assets/tabIcon/search.png');
                    }

                    // You can return any component that you like here!
                    //return <Ionicons name={iconName} size={size} color={color} />;
                    
                    return <Image source={src} style={{width:25,height:25}}/>
                },
            })}
            tabBarOptions={{
                
                activeTintColor: 'black',
                inactiveTintColor: 'gray',
            }}

            >
            <Tab.Screen name="HomeScreen" component={HomeScreen} options={{title :"Home" }}/>
            <Tab.Screen name="SearchScreen" component={SearchScreen} options={{title: "Search" }}/>
        </Tab.Navigator>
    );
}

export default MyTabs;