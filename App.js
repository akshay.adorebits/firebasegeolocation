/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
} from 'react-native';

import AppNavigation from './src/navigation/AppNavigation';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from './src/firebase/firebaseConfig';
import checkUser from './src/firebase/checkUser';

export default class App extends React.Component {
  state={
   // userid : '',
    userid:false,
    userExist : false
  }
  componentDidMount(){
    this._checkLogin();
  }
  
  _checkLogin = async() => {
    await AsyncStorage.getItem('id').then((userid)=>{
      this.setState({userid:userid})
      checkUser(this.state.userid).then((data)=>{
        this.setState({userExist:data});
      })
   
    });
  }

  render() {
   
    return (
      <AppNavigation user={this.state.userid} userExist={this.state.userExist}/>
    )
  }
}
